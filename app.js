// features/4-expose-all-configuration-as-environment-variable
const express = require('express')
const app = express()
/** [FB041920]/feature_3: Changing port number */
const port = 3000

app.get('/', (req, res) => {
    res.send('Hello World!');
    console.log('Response generated...');
});

app.get('/hi', (req, res) => {
    res.send('Hello World! Hi there');
    console.log('Response generated...');
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))